package AI;

import com.rational.progress.Battle;
import com.rational.progress.GameManager;

import java.util.ArrayList;
import java.util.PriorityQueue;

import board.Board;
import board.Territory;
import player.Player;

public class ProgressAI {

	private Player p;
	private Board b;
	private GameManager gm;
	private Territory[][] map;
	private Battle battle;
	private float warning = 0;

	public static final int devCap = 10; // if you change this, remember to
											// change it in GameManager too!!

	public ProgressAI(Player p, Board b, GameManager gm) {
		this.p = p;
		this.b = b;
		this.gm = gm;
		map = b.getMap();
	}

	public void doTurn() {
		System.out.println("my threat" + this.getAIThreatLevel());
		if (isDraftPhase()) {
			// TODO
			System.out.println("Save me");

		} else if (isGamePhase()) {
			if (shouldConquer()) {

				ArrayList<Territory> importantTerritories = importantEnemyTerritories();
				ArrayList<Territory> path = getOptimalPathThroughTerritories();

				Territory at = path.get(0);

				placeTerritories(at);

				for (int i = 1; i < path.size(); i++) {

					if (isGoodIdeaToAttack(at, path.get(i))) {
						attack(at, path.get(i));
						at = path.get(i);
					} else {
						break;
					}
				}
			} else { // expand
				if (shouldFindTriforces()) {

					ArrayList<Territory> triforces = getTriforces();
					sortTriforcesByEase(triforces);
					Territory at = getClosestBorderToTerritory(triforces.get(0));
					placeTerritories(at);

					boolean shouldContinue = true;

					for (Territory ct : triforces) {
						ArrayList<Territory> triforce = getTriforce(ct);

						for (Territory t : triforce) {
							AStar astar = new AStar(b);
							ArrayList<Territory> path = astar.getPath(at, t);

							for (Territory step : path) {

								if (isGoodIdeaToAttack(at, step)) {
									attack(at, step);
									at = step;
								} else {
									shouldContinue = false;
									break;
								}
							}

							if (!shouldContinue) {
								break;
							}
						}

						if (!shouldContinue) {
							break;
						}

						sortTriforcesByEase(triforces);
					}
				} else { // build walls
					ArrayList<Territory> ts = turtleAlgorithm();
					for (Territory t : ts) {
						placeTerritories(t, getNumTerritoryTurtle(t));
					}
				}
			}
		}
	}

	private void attack(Territory a, Territory d) {
		battle = new Battle();
		battle.calculate(a.getArmyCount() - 1, d.getArmyCount());

		gm.battleGUIInput(battle.getWin(), battle, 0);

	}

	private ArrayList<Territory> importantEnemyTerritories() {
		ArrayList<Territory> important = new ArrayList<Territory>();

		for (int i = 0; i < map.length; i++) {
			for (int k = 0; k < map[0].length; k++) {

				// if the enemy territory is developed or will be developed
				// in its next turn
				if (map[i][k] instanceof Territory
						&& map[i][k].getPlayerID() != p.getPlayerID()) {

					int temp = b.getNumberOfOwnedAdjacentTerritories(i, k,
							p.getPlayerID());

					if (temp == 2 && map[i][k].getDevelopment() >= devCap - 1) {
						important.add(map[i][k]);

					}

					else if (temp == 3
							&& map[i][k].getDevelopment() >= devCap - 3) {
						important.add(map[i][k]);
					}
				}
			}
		}

		return important;
	}

	private ArrayList<Territory> getOptimalPathThroughTerritories() {
		ArrayList<Territory> aiTerr = getAITerritories();
		ArrayList<Territory> borders = getAIBorderingTerritories();
		int largest = 0;
		Territory valuableTerr = null;
		for (Territory t : aiTerr) {
			if (borders.contains(t) && t.getArmyCount() > largest) {
				valuableTerr = t;
				largest = t.getArmyCount();
			}
		}
		if (valuableTerr == null) {
			return null;
		}
		ArrayList<Territory> enemy = importantEnemyTerritories();
		largest = 0;
		Territory impEn = enemy.get(0);
		for (Territory t : enemy) {
			if (t.getDevelopment() > largest) {
				largest = t.getDevelopment();
				impEn = t;
			}
		}
		AStar astar = new AStar(b);
		ArrayList<Territory> path = astar.getPath(valuableTerr, impEn);
		return path;
	}

	private float getAIThreatLevel() {
		ArrayList<Territory> aiTerr = getAITerritories();
		ArrayList<Territory> borders = getAIBorderingTerritories();
		int largest = 0;
		Territory valuableTerr = null;
		for (Territory t : aiTerr) {
			if (borders.contains(t) && t.getArmyCount() > largest) {
				valuableTerr = t;
				largest = t.getArmyCount();
			}
		}
		if (valuableTerr == null) {
			return -1f;
		}
		ArrayList<Territory> enemy = importantEnemyTerritories();
		largest = 0;

		if (enemy.isEmpty()) {
			return -1;
		}

		Territory impEn = enemy.get(0);
		for (Territory t : enemy) {
			if (t.getDevelopment() > largest) {
				largest = t.getDevelopment();
				impEn = t;
			}
		}
		AStar astar = new AStar(b);
		ArrayList<Territory> path = astar.getPath(valuableTerr, impEn);
		Battle b = new Battle();
		float odds = 1f;
		Territory pt = null;
		for (Territory t : path) {
			if (pt == null) {
				pt = t;
			} else {
				b.calculate(pt.getArmyCount() - 1, t.getArmyCount());
				odds *= b.getBen();
			}
		}
		return odds;
	}

	private ArrayList<Territory> turtleAlgorithm() {
		ArrayList<Territory> borders = getAIBorderingTerritories();
		sortTerritoriesBasedOnEnemyDistance(borders);
		return borders;
	}

	private void sortTerritoriesBasedOnEnemyDistance(ArrayList<Territory> ts) {

		if (!ts.isEmpty()) {

			// remove impossible to reach enemies first
			for (int j = 0; j < ts.size(); j++) {
				if (getDistanceToClosestEnemyTerritory(ts.get(j)) < 0) {
					ts.remove(j);
					j--;
				}
			}

			// bubble sort!! :D
			for (int i = 0; i < ts.size(); i++) {
				for (int k = 1; k < ts.size() - i; k++) {
					if (getDistanceToClosestEnemyTerritory(ts.get(k - 1)) > getDistanceToClosestEnemyTerritory(ts
							.get(k))) {
						Territory superTemp = ts.get(k);
						ts.set(k, ts.get(i));
						ts.set(i, superTemp);

					}

				}

			}

		}

	}

	private Territory getClosestEnemyTerritory(Territory ot) {
		ArrayList<Territory> et = getEnemyTerritories();
		int shortest = Integer.MAX_VALUE;
		Territory closest = null;
		for (Territory t : et) {
			AStar astar = new AStar(b);
			ArrayList<Territory> path = astar.getPath(t, ot);
			if (path != null && path.size() < shortest) {
				shortest = path.size();
				closest = t;
			}
		}
		return closest;
	}

	private int getDistanceToClosestEnemyTerritory(Territory ot) {
		ArrayList<Territory> et = getEnemyTerritories();
		int shortest = Integer.MAX_VALUE;
		int count = 0;

		for (Territory t : et) {
			AStar astar = new AStar(b);
			ArrayList<Territory> path = astar.getPath(t, ot);
			if (path == null) {
				count++;
			} else if (path.size() < shortest) {
				shortest = path.size();
			}
		}
		if (count == et.size()) {
			return -1;
		}
		return shortest;
	}

	private ArrayList<Territory> getEnemyTerritories() {
		ArrayList<Territory> et = new ArrayList<Territory>();
		Territory[][] map = b.getMap();
		for (int i = 0; i < map.length; i++) {
			for (int c = 0; c < map[0].length; c++) {
				if (map[i][c] instanceof Territory
						&& !(map[i][c].getPlayerID() == p.getPlayerID() || map[i][c]
								.getPlayerID() == 0)) {
					et.add(map[i][c]);
				}
			}
		}
		return et;
	}

	private int getNumTerritoryTurtle(Territory t) {
		if (t.getArmyCount() > 5) {
			return 0;
		} else {
			return 3; // work in some sort of thing to prevent placing too many
						// perhaps in gamemanager class
		}
	}

	private boolean shouldFindTriforces() { // TODO

		float threatLevel = getAIThreatLevel();

		if (threatLevel < .4) {
			return true;
		}

		return false;
	}

	private Territory getClosestBorderToTerritory(Territory ct) {
		ArrayList<Territory> borders = getAIBorderingTerritories();
		int shortest = Integer.MAX_VALUE;
		Territory closest = null;
		for (Territory t : borders) {
			AStar astar = new AStar(b);
			ArrayList<Territory> path = astar.getPath(t, ct);
			if (path.size() < shortest) {
				shortest = path.size();
				closest = t;
			}
		}
		return closest;
	}

	// Precondition - Territory t is owned by the AI
	// if this precondition is not true
	// just add a check to make sure the Territory is owned by the AI
	private void placeTerritories(Territory t) {
		t.setArmyCount(t.getArmyCount() + gm.getPlacementArmies());
		gm.setPlacementArmies(0);
	}

	// Precondition - Territory t is owned by the AI
	// if this precondition is not true
	// just add a check to make sure the Territory is owned by the AI

	// Precondition - numTer is not more than the number of armies that can be
	// placed
	private void placeTerritories(Territory t, int numTer) {
		t.setArmyCount(t.getArmyCount() + numTer);
		gm.setPlacementArmies(gm.getPlacementArmies() - numTer);
	}

	private boolean isGamePhase() {
		if (gm.getPhase() == 0) {
			return false;
		}
		return true;
	}

	private boolean isDraftPhase() {
		if (gm.getPhase() == 0) {
			return true;
		}
		return false;
	}

	private boolean shouldConquer() {
		return (getAIThreatLevel() > .5f); // aggressive level of AI, may change
											// later
	}

	private ArrayList<Territory> getTriforces() {
		ArrayList<Territory> tfs = new ArrayList<Territory>();
		Territory[][] map = b.getMap();
		for (int i = 0; i < map.length; i++) {
			for (int c = 0; c < map[0].length; c++) {
				if (map[i][c] instanceof Territory) {
					if (b.getAdjacentTerritories(i, c).size() == 3) {
						tfs.add(map[i][c]);
					}
				}
			}
		}
		return tfs;
	}

	private ArrayList<Territory> getTriforce(Territory t) { // takes a point and
															// returns all
															// territories in
															// the triforce
		ArrayList<Territory> tf = new ArrayList<Territory>();
		tf.add(t);
		tf.addAll(b.getAdjacentTerritories((int) t.getX(), (int) t.getY()));

		return tf;
	}

	private void sortTriforcesByEase(ArrayList<Territory> ts) {
		PriorityQueue<Territory> sorted = new PriorityQueue<Territory>();
		AStar ease = new AStar(b);

		// finds the ones that are easiest
		// if ones that own part of, top of the list?
		// otherwise sort by distance and army number
		// calculations from A*

		// for Territory t : ts
		// run heuristic

		// filters out stuff owned by AI
		// destroys array and adds things to a temporary PriorityQueue
		for (int i = 0; i < ts.size(); i++) {
			ArrayList<Territory> temp = getTriforce(ts.get(i));
			int ownCount = 0;

			for (Territory t : temp) {
				if (t.getPlayerID() == p.getPlayerID()) {
					ownCount++;
				}
			}

			// maybe make it higher priority if AI is closer to taking it?
			// TODO
			// if you need to edit heur for things like partial owns
			// do it here

			// triforces not owned completely by the AI, add it to list for
			// sorting
			if (ownCount < 4) {
				sorted.add(ts.get(i));
			}

			ts.remove(i);
			i--;
		}

		// rebuilds the original ArrayList, now sorted
		while (!sorted.isEmpty()) {
			ts.add(sorted.remove());
		}

	}

	private boolean isGoodIdeaToAttack(Territory a, Territory d) {
		Battle b = new Battle();
		b.calculate(a.getArmyCount() - 1, d.getArmyCount());
		if (b.getBen() > .5) {
			return true;
		}
		return false;
	}

	private ArrayList<Territory> getAITerritories() {
		ArrayList<Territory> ait = new ArrayList<Territory>();
		Territory[][] map = b.getMap();
		for (int i = 0; i < map.length; i++) {
			for (int c = 0; c < map[0].length; c++) {
				if (map[i][c] instanceof Territory) {
					if (map[i][c].getPlayerID() == p.getPlayerID()) {
						ait.add(map[i][c]);
					}
				}
			}
		}
		return ait;
	}

	private ArrayList<Territory> getAIBorderingTerritories() {
		ArrayList<Territory> terr = getAITerritories();
		for (int i = 0; i < terr.size(); i++) {
			boolean shouldKeep = false;
			ArrayList<Territory> adj = b.getAdjacentTerritories(
					(int) terr.get(i).getX(), (int) terr.get(i).getY());

			for (Territory t : adj) {
				if (t.getPlayerID() != p.getPlayerID()) {
					shouldKeep = true;
				}
			}
			if (!shouldKeep) {
				terr.remove(i);
				i--;
			}
		}
		return terr;
	}

	public void setGM(GameManager gm) {
		this.gm = gm;
	}

}