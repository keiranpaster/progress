package AI;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import board.Board;
import board.Territory;

public class AStar {

	private Board b;

	public AStar(Board b) {
		this.b = b;
	}

	public ArrayList<Territory> getPath(Territory t1, Territory t2) {
		return getPath((int) t1.getX(), (int) t1.getY(), (int) t2.getX(),
				(int) t2.getY());
	}

	public ArrayList<Territory> getPath(int x1, int y1, int x2, int y2) { // someone
																			// implement
																			// A*
																			// here
		System.out.println("Hello World!");

		Set<ProgressNode> explored = new HashSet<ProgressNode>();

		ArrayList<Territory> adj = b.getAdjacentTerritories(x1, y1);
		ArrayList<ArrayList<ProgressNode>> path = new ArrayList<ArrayList<ProgressNode>>();
		ArrayList firstT = new ArrayList();
		firstT.add(new ProgressNode(0, b.getMap()[x1][y1]));
		path.add(firstT);
		boolean ReachedGoal = false;
		ArrayList<ProgressNode> SmallPath = null;
		while (!(ReachedGoal)) {
			for (int c = 0; c < path.size() && c < 7; c++) {
				ArrayList<ProgressNode> CurrPath = ShortestPath(path);

				Territory ThePath = CurrPath.get(CurrPath.size() - 1).T;

				adj = b.getAdjacentTerritories((int) (ThePath.getX()),
						(int) (ThePath.getY()));
				int ShortestPath = shortestPathIndex(path);

				for (int i = 0; i < adj.size(); i++) {
					Territory curr = adj.get(i);
					if (curr.getPlayerID() == b.getMap()[x1][y1].getPlayerID()) {
						continue;
					}
					ProgressNode Exploration = hasExplored(explored, curr);
					if (Exploration != null) {
						if (curr.getArmyCount()
								+ CurrPath.get(CurrPath.size() - 1).length < Exploration.length) {
							Exploration = CurrPath.get(CurrPath.size() - 1);
						}
					} else {

						ProgressNode Progress = new ProgressNode(
								CurrPath.get(CurrPath.size() - 1).length
										+ curr.getArmyCount(), curr);
						explored.add(Progress);
						// CurrPath.add(Progress);
						ArrayList<ProgressNode> SmallestPath = new ArrayList<ProgressNode>(
								CurrPath);
						SmallestPath.add(Progress);
						path.add(SmallestPath);
						SmallPath = SmallestPath;
						if (curr.getX() == x2 && curr.getY() == y2) {
							ReachedGoal = true;

						}
					}

					int aCount = curr.getArmyCount();

				}
				path.remove(ShortestPath);
			}
		}
		if (SmallPath == null) {
			return null;
		}
		ArrayList<Territory> finalPath = new ArrayList<Territory>();
		for (int k = 0; k < SmallPath.size(); k++) {
			finalPath.add(SmallPath.get(k).T);
		}

		return finalPath;
	}

	private int shortestPathIndex(ArrayList<ArrayList<ProgressNode>> Paths) {
		int count;
		int shortest = 10000;
		int index = -1;
		for (int i = 0; i < Paths.size(); i++) {
			// Paths.get(i).get(Paths.get(i).size()-1).length;
			count = Paths.get(i).get(Paths.get(i).size() - 1).length;
			if (count < shortest) {
				shortest = count;
				index = i;

			}
		}
		return index;
	}

	private ProgressNode hasExplored(Set<ProgressNode> ex, Territory t) {
		for (ProgressNode n : ex) {
			if (n.T.equals(t)) {
				return n;

			}

		}
		return null;
	}

	private ArrayList<ProgressNode> ShortestPath(
			ArrayList<ArrayList<ProgressNode>> Paths) {
		int count;
		int shortest = 10000;
		ArrayList<ProgressNode> ShortestPath = null;
		for (int i = 0; i < Paths.size(); i++) {
			// Paths.get(i).get(Paths.get(i).size()-1).length;
			count = Paths.get(i).get(Paths.get(i).size() - 1).length;
			if (count < shortest) {
				shortest = count;
				ShortestPath = Paths.get(i);

			}
		}
		return ShortestPath;
	}

}
