package com.rational.progress;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import player.Player;

import java.awt.Font;

public class SetupState extends BasicGameState {
	private Rectangle button1;
	private Rectangle button2playerselect;
	private Rectangle button3playerselect;
	private Rectangle button4playerselect;

	private Rectangle button5Red;
	private Rectangle button6Green;
	private Rectangle button7Yellow;
	private Rectangle button8Blue;

	private Rectangle buttonBack;

	public static int redControl;
	public static int greenControl;
	public static int yellowControl;
	public static int blueControl;
	public static int playerControl;

	Font font;
	Font font1;
	TrueTypeFont ttf;
	TrueTypeFont ttf2;

	boolean infoRed;
	boolean infoGreen;
	boolean infoYellow;
	boolean infoBlue;

	Image background;
	Image infoRtext;
	Image infoGtext;
	Image infoYtext;
	Image infoBtext;

	public SetupState(int id) {
		button1 = new Rectangle(600, 475, 150, 50);

		button2playerselect = new Rectangle(230, 80, 100, 40);
		button3playerselect = new Rectangle(340, 80, 100, 40);
		button4playerselect = new Rectangle(450, 80, 100, 40);

		button5Red = new Rectangle(100, 160, 200, 40);
		button6Green = new Rectangle(500, 160, 200, 40);
		button7Yellow = new Rectangle(100, 300, 200, 40);
		button8Blue = new Rectangle(500, 300, 200, 40);

		buttonBack = new Rectangle(50, 475, 150, 50);

		infoRed = false;
		infoGreen = false;
		infoBlue = false;
		infoYellow = false;

		redControl = 1;
		greenControl = 1;
		yellowControl = 1;
		blueControl = 1;
		playerControl = 1;

	}

	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		// TODO Auto-generated method stub

		font = new Font("Verdana", Font.BOLD, 24);
		font1 = new Font("Verdana", Font.BOLD, 16);
		ttf = new TrueTypeFont(font, true);
		ttf2 = new TrueTypeFont(font1, true);
		background = new Image("res/setupmenu.jpg");
		infoRtext = new Image("res/redText.jpg");
		infoGtext = new Image("res/greenText.jpg");
		infoYtext = new Image("res/yellowText.jpg");
		infoBtext = new Image("res/blueText.jpg");

	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		// TODO Auto-generated method stub

		if (background != null) {
			background.draw(0, 0);
		}

		// playstate button
		Color gray = Color.gray;
		gray.a = .8f;
		g.setColor(gray);
		gray.a = 1f;
		g.fill(button1);
		g.draw(button1);
		g.setColor(Color.black);

		ttf.drawString(603, 486, "BEGIN!");

		// back button
		gray = Color.gray;
		gray.a = .8f;
		g.setColor(gray);
		gray.a = 1f;
		g.fill(buttonBack);
		g.draw(buttonBack);
		g.setColor(Color.black);

		ttf.drawString(73, 486, "Back");

		// button to select two player
		Color green = Color.green;
		if (playerControl == 3) {
			green.a = .8f;
		} else {
			green.a = .3f;
		}
		g.setColor(green);
		green.a = 1f;
		g.fill(button2playerselect);
		g.draw(button2playerselect);
		g.setColor(Color.black);

		ttf2.drawString(238, 90, "2 Players");

		// button to select three players
		Color yellow = Color.yellow;
		if (playerControl == 2) {
			yellow.a = .8f;
		} else {
			yellow.a = .3f;
		}
		g.setColor(yellow);
		yellow.a = 1f;
		g.fill(button3playerselect);
		g.draw(button3playerselect);
		g.setColor(Color.black);

		ttf2.drawString(348, 90, "3 Players");

		// button to select four players
		Color blue = Color.blue;
		if (playerControl == 1) {
			blue.a = .8f;
		} else {
			blue.a = .3f;
		}
		g.setColor(blue);
		blue.a = 1f;
		g.fill(button4playerselect);
		g.draw(button4playerselect);
		g.setColor(Color.black);

		ttf2.drawString(458, 90, "4 Players");

		// button to change player1 to comp or hum
		if (redControl == 1) {
			Color red = Player.getColorForPlayerID(1);
			red.a = .8f;
			g.setColor(red);
			red.a = 1f;
			g.fill(button5Red);
			g.draw(button5Red);
			g.setColor(Color.black);

			ttf2.drawString(110, 170, "Human");
		}

		else {
			Color red = (Player.getColorForPlayerID(1));
			red.a = .3f;
			g.setColor(red);
			red.a = 1f;
			g.fill(button5Red);
			g.draw(button5Red);
			g.setColor(Color.black);

			ttf2.drawString(110, 170, "AI");

		}

		if (greenControl == 1) {
			green = Player.getColorForPlayerID(2);
			green.a = .8f;
			g.setColor(green);
			green.a = 1f;
			g.fill(button6Green);
			g.draw(button6Green);
			g.setColor(Color.black);
			ttf2.drawString(510, 170, "Human");
		} else {
			green = Player.getColorForPlayerID(2);
			green.a = .3f;
			g.setColor(green);
			green.a = 1f;
			g.fill(button6Green);
			g.draw(button6Green);
			g.setColor(Color.black);

			ttf2.drawString(510, 170, "AI");

		}
		if (playerControl < 3) {
			/*
			 * yellow = Color.yellow; yellow.a = .8f; g.setColor(yellow);
			 * yellow.a = 1f; g.fill(button7Yellow); g.draw(button7Yellow);
			 * g.setColor(Color.black);
			 */
			if (yellowControl == 1) {
				yellow = Player.getColorForPlayerID(3);
				yellow.a = .8f;
				g.setColor(yellow);
				yellow.a = 1f;
				g.fill(button7Yellow);
				g.draw(button7Yellow);
				g.setColor(Color.black);
				ttf2.drawString(110, 310, "Human");
			} else {
				yellow = Player.getColorForPlayerID(3);
				yellow.a = .3f;
				g.setColor(yellow);
				yellow.a = 1f;
				g.fill(button7Yellow);
				g.draw(button7Yellow);
				g.setColor(Color.black);
				ttf2.drawString(110, 310, "AI");
			}

			/*
			 * blue = Color.blue; blue.a = .8f; g.setColor(blue); blue.a = 1f;
			 * g.fill(button8Blue); g.draw(button8Blue);
			 * g.setColor(Color.black);
			 */
			if (playerControl < 2) {
				if (blueControl == 1) {
					blue = Player.getColorForPlayerID(4);
					blue.a = .8f;
					g.setColor(blue);
					blue.a = 1f;
					g.fill(button8Blue);
					g.draw(button8Blue);
					g.setColor(Color.black);
					ttf2.drawString(510, 310, "Human");
				}

				else {
					blue = Player.getColorForPlayerID(4);
					blue.a = .3f;
					g.setColor(blue);
					blue.a = 1f;
					g.fill(button8Blue);
					g.draw(button8Blue);
					g.setColor(Color.black);
					ttf2.drawString(510, 310, "AI");
				}
			}
		}
		if (infoRed) {
			g.drawImage(infoRtext, 100, 210);
		}
		if (infoGreen) {
			g.drawImage(infoGtext, 500, 210);
		}
		if (!(playerControl >= 3)) {
			if (infoYellow) {
				g.drawImage(infoYtext, 100, 350);
			}

		}
		if (playerControl == 1) {

			if (infoBlue) {
				g.drawImage(infoBtext, 500, 350);
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		// TODO Auto-generated method stub
		Input i = gc.getInput();
		if (button1.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				// System.out.println("123123");
				sbg.enterState(1, new FadeOutTransition(),
						new FadeInTransition());
			}
		}
		if (buttonBack.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				// System.out.println("123123");
				sbg.enterState(0, new FadeOutTransition(),
						new FadeInTransition());
			}
		}
		if (button2playerselect.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				playerControl = 3;// 2
			}
		}
		if (button3playerselect.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				playerControl = 2;// 3
			}
		}
		if (button4playerselect.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				playerControl = 1;// 4
			}
		}
		if (button5Red.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				redControl = redControl * (-1);

			}

		}
		if (button6Green.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				greenControl = greenControl * (-1);

			}

		}
		if (button7Yellow.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				yellowControl = yellowControl * (-1);

			}

		}
		if (button8Blue.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				blueControl = blueControl * (-1);

			}

		}
		if (button5Red.contains(i.getMouseX(), i.getMouseY())) {
			infoRed = true;
		}
		if (!(button5Red.contains(i.getMouseX(), i.getMouseY()))) {
			infoRed = false;
		}

		if (button6Green.contains(i.getMouseX(), i.getMouseY())) {
			infoGreen = true;
		}
		if (!(button6Green.contains(i.getMouseX(), i.getMouseY()))) {
			infoGreen = false;
		}

		if (button7Yellow.contains(i.getMouseX(), i.getMouseY())) {
			infoYellow = true;
		}
		if (!(button7Yellow.contains(i.getMouseX(), i.getMouseY()))) {
			infoYellow = false;
		}

		if (button8Blue.contains(i.getMouseX(), i.getMouseY())) {
			infoBlue = true;
		}
		if (!(button8Blue.contains(i.getMouseX(), i.getMouseY()))) {
			infoBlue = false;
		}

	}

	@Override
	public int getID() {
		return 2;
	}

}
