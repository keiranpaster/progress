package com.rational.progress;

public class Battle {
	private int aStrength;
	private int dStrength;
	private int winCount;
	private int loseCount;
	private int modAArmy;
	private int modDArmy;
	private boolean win;
	private double tieBreaker;

	private double ben; // chance of winning battle
	private int wCouldLose;// how much you(attacker) could lose if you win
	private int lCouldLose;// how much you(attacker) could lose if you lose
	private int oppCouldLoseL;// how much you(defender) could lose if attacker
								// loses
	private int oppCouldLoseW;// how much you(defender) could lose if attacker
								// wins

	private int newArmyCount;
	private int oppNewArmyCount;

	public Battle() {
		aStrength = 0;
		dStrength = 0;
		winCount = 0;
		loseCount = 0;
		modAArmy = 0;
		modDArmy = 0;
		win = false;
		tieBreaker = 0;
		ben = 0;
		wCouldLose = 0;
		lCouldLose = 0;
		oppCouldLoseL = 0;
		oppCouldLoseW = 0;
		newArmyCount = 0;
		oppNewArmyCount = 0;
	}

	public void calculate(int a, int b) {
		int aArmy = a;
		int dArmy = b;

		modifyArmies(aArmy, dArmy);
		calculateLosses(aArmy, dArmy);

		// runs the calculation for every ten men both att and def have to make
		// the battles make more sense and less random victories or defeat
		for (int i = 0; ((i <= aArmy) && (i <= dArmy)); i = i + 10) {
			calculateCounts();
		}

		if (winCount > loseCount) {
			win = true;
		} else if (loseCount > winCount) {
			win = false;
		} else {
			tieBreaker = (int) (Math.random() * 100 + 0.5);
			if (tieBreaker > 50) {
				win = true;
			} else {
				win = false;
			}
		}

		if (win && aArmy > 1) {
			newArmyCount = aArmy - (int) (dArmy * (Math.random() * .5 + .51));
			oppNewArmyCount = dArmy
					- (int) (modAArmy * (Math.random() * .4 + .25));

		}

		// special case where it's 1 v 1, attempt at 50% chance
		else if (win && aArmy == 1 && dArmy == 1) {
			int dec = (int) (Math.random() * 100 + 1.5);
			if (dec > 50) {
				newArmyCount = 1;
				oppNewArmyCount = 0;
			} else {
				newArmyCount = 0;
				oppNewArmyCount = 1;
			}
		}

		else {

			newArmyCount = aArmy - (int) (dArmy * (Math.random() * .4 + .25));
			oppNewArmyCount = dArmy
					- (int) (modAArmy * (Math.random() * .5 + .51));

		}

	}

	private void modifyArmies(int aArmy, int dArmy) {
		// calculates modified armies
		if (dArmy >= aArmy) {
			modDArmy = (int) (dArmy * 1.05);

			modAArmy = aArmy;

			ben = ((double) (dArmy / 2.0 / aArmy));// chance of winning battle
		}
		// this gives the defender 5% more men if the defender has more men or
		// the two forces are equal
		else {
			modAArmy = (int) (aArmy * 1.05);

			modDArmy = dArmy;

			ben = ((double) 1 - (dArmy / 2.0 / aArmy));// chance of winning
														// battle
		}
		// this gives the attacker 5% more men if the defender has more men
	}

	private void calculateLosses(int aArmy, int dArmy) {
		wCouldLose = (int) (dArmy * .25);// if att wins, def could lose (this is
		// the minimum, the maximum is
		// darmy)
		lCouldLose = (int) (dArmy * .51);// if att loses, def could lose (this
		// is the minimum, the maximum is
		// darmy)
		oppCouldLoseW = (int) (aArmy * .51);// /if def wins, att could lose
		// (this is the minimum, the maximum
		// is darmy)
		oppCouldLoseL = (int) (aArmy * .25);// /if def loses, att could lose
		// (this is the minimum, the maximum
		// is darmy)
	}

	// Pre-condition: modified armies are not 0
	private void calculateCounts() {

		aStrength = (int) (modAArmy * 10 * (Math.random() + 1));
		dStrength = (int) (modDArmy * 10 * (Math.random() + 1));

		// this is where the probability of wining or losing comes into
		// effect since it calculates the " strength " of each side

		if (aStrength > dStrength) {
			winCount++;

		}
		// if the attacker wins that battle the win count increases

		else {
			loseCount++;
			// if the attacker wins that battle the win count increases

		}

	}

	public double getBen() {
		return ben;
	}

	public boolean getWin() {
		return win;
	}
	
	public int getwCouldLose() {
		return wCouldLose;
	}

	public int getlCouldLose() {
		return lCouldLose;
	}

	public int getOppCouldLoseL() {
		return oppCouldLoseL;
	}

	public int getOppCouldLoseW() {
		return oppCouldLoseW;
	}

	public int getNewArmyCount() {
		return newArmyCount;
	}

	public int getOppNewArmyCount() {
		return oppNewArmyCount;
	}

}
