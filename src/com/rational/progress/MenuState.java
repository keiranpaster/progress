package com.rational.progress;



import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.awt.Font;


public class MenuState extends BasicGameState {
	
	private Rectangle button1;

	Font font;
	TrueTypeFont ttf;
	Image land;
	Font font2;
	TrueTypeFont ttf3;
	
	public MenuState(int id) {
	
		
		button1 = new Rectangle(300, 175, 200, 60);
		//reformRectangle();
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		// TODO Auto-generated method stub
		
		  font = new Font("Verdana", Font.BOLD, 24);
		  ttf = new TrueTypeFont(font, true);
		  land = new Image("res/menuV2.jpg");
		  font2 = new Font("Verdana", Font.ITALIC , 34);
		  ttf3 = new TrueTypeFont(font2, true);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		// TODO Auto-generated method stub
		
		if (land != null) {
		    land.draw(0,0);
		}
	
		
		Color gray = Color.gray;
		gray.a = .6f;
		g.setColor(gray);
		gray.a = 1f;
		g.fill(button1);
		g.draw(button1);
		g.setColor(Color.black); 

		ttf.drawString(355, 186, "BEGIN!");
		ttf3.drawString(350, 30, "Progress!");
		
		
//		 g.setColor(Color.white);
//		    g.drawString("Higher or Lower", 50, 10);
//		 
//		    g.drawString("1. Play Game", 50, 100);
//		    g.drawString("2. High Scores", 50, 120);
//		    g.drawString("3. Quit", 50, 140);
//		    
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		// TODO Auto-generated method stub
		Input i = gc.getInput();
		if (button1.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				//System.out.println("123123");
				sbg.enterState(2, new FadeOutTransition(), new FadeInTransition());
			}
		}
	}

	@Override
	public int getID() {
		return 0;
	}

	
	
}
