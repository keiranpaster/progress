package com.rational.progress;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import player.Player;
import AI.ProgressAI;
import GUI.AttackGUI;
import GUI.GameContinueButton;
import GUI.InstructionZone;
import GUI.NumberSelector;
import board.*;

public class PlayState extends BasicGameState {

	private Board board;
	private Player[] players;
	private int numPlayers = 3;
	private InstructionZone iz;
	private GameManager gm;
	private ProgressAI ai;

	public AttackGUI agui;
	public NumberSelector ns;
	private GameContinueButton gcb;

	// private Music mainMusic;

	public PlayState(int id) {
		board = new Board(4);
	}

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {

	}

	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		// mainMusic = new Music("res/battleMusic.ogg");
		numPlayers = 5 - SetupState.playerControl;
		players = new Player[numPlayers];
		for (int i = 0; i < numPlayers; i++) {
			players[i] = new Player(i + 1, players.length, 800);

			if ((i == 0 && SetupState.redControl != 1)
					|| (i == 1 && SetupState.greenControl != 1)
					|| (i == 2 && SetupState.yellowControl != 1)
					|| (i == 3 && SetupState.blueControl != 1)) {
				players[i].setAI(true);
				players[i].setTheAI(new ProgressAI(players[i], board, gm));
			}
		}
		iz = new InstructionZone();
		iz.setInstructions("Draft Phase! Pick your starting territories, player 1!");

		gm = new GameManager(this);

		for (int i = 0; i < players.length; i++) {
			if (players[i].getIsAI()) {
				players[i].getTheAI().setGM(gm);
			}
		}

		agui = new AttackGUI(gm);
		ns = new NumberSelector(gm);
		// sample code kappa
		// ns.create(0, 10, 0); //agui.showAttackGUI(null, null);
		gcb = new GameContinueButton();
		gcb.show();
		gcb.setGM(gm);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		// if(!mainMusic.playing())
		// mainMusic.loop();
		board.render(gc, g);
		for (int i = 0; i < players.length; i++) {
			players[i].renderPlayerBox(gc, g);
		}
		iz.render(gc, g);
		agui.render(gc, g);
		ns.render(gc, g);
		gcb.render(gc, g);

	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		agui.update(gc, delta);
		ns.update(gc, delta);
		board.update(gc, delta);
		for (int i = 0; i < players.length; i++) {
			players[i].update(gc, delta);
		}
		gcb.update(gc, delta);
	}

	@Override
	public int getID() {
		return 1;
	}

	public void setIZSet(String str) {
		iz.setInstructions(str);
	}

	public Board getBoard() {
		return board;
	}

	public Player[] getPlayers() {
		return players;
	}

}
