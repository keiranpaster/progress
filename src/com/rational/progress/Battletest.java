package com.rational.progress;

public class Battletest {

	public static void main(String[] args) {
		int Aarmy = 31;
		int Darmy = 30;
		int Astrength;
		int Dstrength;
		int wincount = 0;
		int losecount = 0;
		int modAarmy = 0;
		int modDarmy = 0;
		int num = 0;
		double tiebreaker = 0;
		
		

		// TODO Auto-generated method stub
		System.out.println("the Attacker before " + Aarmy);
		System.out.println("the Defender before " + Darmy);
		if (Darmy >= Aarmy) {
			modDarmy = (int) (Darmy * 1.05);
			
			modAarmy = Aarmy;
		}
		// this gives the defender 10% more men if the defender has more men or
		// the two forces are equal
		else {
			modAarmy = (int) (Aarmy * 1.05);
			modDarmy = Darmy;
		}
		
		System.out.println ( "chance of winning battle " + ((double) 1-(Darmy / 2.0 / Aarmy)) );
		System.out.println ( " if you win you could lose " + (int)(Darmy * .25) + " - " + Darmy);
		System.out.println ( " if you lose you could lose " + (int)(Darmy * .51) + " - " + Darmy);
		System.out.println ( " if you win opposition could lose " + (int)(Aarmy * .51) + " - " + Aarmy);
		System.out.println ( " if you lose opposition could lose " + (int)(Aarmy * .21) + " - " + Aarmy);
		
		// this gives the attacker 10% more men if the defender has more men

		// runs the calculation for every ten men both att and def have to make
		// the battles make more sense and less random victories or defeat
		for (int i = 0; ((i < Aarmy) && (i < Darmy)); i = i + 10) {
			Astrength = (int) (modAarmy * Math.random() );
			Dstrength = (int) (modDarmy * Math.random() );
			// this is where the probability of wining or losing comes into
			// effect since it calculates the " strength " of each side
			System.out.println(Astrength + "a");
			System.out.println(Dstrength + "d");
			if (Astrength > Dstrength) {
				wincount++;

			}
			// if the attacker wins that battle the win count increases

			else {
				losecount++;
				// if the attacker wins that battle the win count increases
			}
		}
		if (wincount > losecount) {
			num = 1;
		} 
		else if (losecount > wincount)
		{
			num = 2;
		}
		else 
		{
			tiebreaker = Math.random();
			if (tiebreaker > .5){
				num = 1;
			}
			else {
				num = 2;
			}
		}
		
		if (num == 1) {
			Aarmy = Aarmy - (int) (Darmy * (Math.random() * .80 + .25));
			Darmy = Darmy - (int) (modAarmy * (Math.random() * .5 + .51));
			System.out.println("Attacker wins ");
			System.out.println("Attacker After " + Aarmy);
			System.out.println("Defender After" + Darmy);
		} else if (num == 2) {
			Aarmy = Aarmy - (int) (Darmy * (Math.random() * .5 + .51));
			Darmy = Darmy - (int) (modAarmy * (Math.random() * .80 + .25));
			System.out.println("Defender wins ");
			System.out.println("Attacker After " + Aarmy);
			System.out.println("Defender After" + Darmy);
		}

	}

}
