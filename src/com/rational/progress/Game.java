package com.rational.progress;

import org.newdawn.slick.state.*;
import org.newdawn.slick.*;

public class Game extends StateBasedGame {

	public static final String gamename = "Progress";
	public static final int menu = 0;
	public static final int play = 1;
	public static final int setup = 2;

	static AppGameContainer appgc;
	
	public Game(String name) {
		super(name);
		this.addState(new MenuState(menu));
		this.addState(new SetupState(setup));
		this.addState(new PlayState(play));
	}

	public static void main(String[] args) {
		try {
			appgc = new AppGameContainer(new Game(gamename));
			appgc.setDisplayMode(800, 600, false);
			appgc.setTargetFrameRate(60);
			appgc.setVSync(true);
			appgc.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		gc.setShowFPS(false);
		this.getState(menu).init(gc, this);
		this.getState(play).init(gc, this);
		this.getState(setup).init(gc, this);
		this.enterState(menu);
	}

}
