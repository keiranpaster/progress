package com.rational.progress;

import java.util.ArrayList;

import player.Player;
import GUI.InstructionZone;
import board.Board;
import board.Territory;

public class GameManager {

	private PlayState playState;
	private Board board;
	private Player[] players;
	private Territory[][] map;
	private int pointer; // points at current Player to play

	private ArrayList<Territory> action; // holds actions
	private int placementArmies;

	private int phase; // 0 = draft, 1 = placement, 2 = battle, 3 = move, 4 =
						// win
	private int draftCounter;

	private Player currentPlayer;

	private String inst;

	private ArrayList<Territory> locked;

	private boolean needsNumberInput = false;
	private boolean needsBattleInput = false;
	private int totalArmyStore = 0;
	private Territory originT;
	private Territory destT;

	private int originTArmy;
	private int destTArmy;

	public static final int devCap = 10;

	public GameManager(PlayState ps) {

		playState = ps;
		board = playState.getBoard();
		players = playState.getPlayers();
		map = board.getMap();

		pointer = -1; // must be this to make the first "advanceTurn()" work
		action = new ArrayList<Territory>();

		// adds the GameManager object to every single Territory
		for (int i = 0; i < map.length; i++) {
			for (int k = 0; k < map[0].length; k++) {
				if (map[i][k] instanceof Territory) {
					map[i][k].setGM(this);
				}

			}
		}

		locked = new ArrayList<Territory>();

		placementArmies = 0;
		phase = 0;
		draftCounter = players.length * 3;
		inst = "";
		advanceTurn();

	}

	public void territoryClick(Territory t) {
		if (!needsNumberInput && !needsBattleInput) {
			System.out
					.println(board.getNumberOfOwnedAdjacentTerritories(
							(int) t.getX(), (int) t.getY(),
							currentPlayer.getPlayerID()));

			// draft phase
			if (draftCounter > 0) {

				System.out.println("Draft phase");

				if (placeDraftArmy(t)) {
					advanceTurn();
				}

				if (draftCounter <= 0) {
					phase = 1;
					setMessage();
				}

			}

			// placement phase
			else if (currentPlayer.getArmyGain() - placementArmies > 0) {
				System.out.println("Placement phase");
				placeArmy(t);

			}

			// add action
			else if (!action.contains(t)) {

				if (phase == 2) {
					// forces first territory to be yours and have more than one
					// army
					if (action.size() < 1 && myTerritory(t)
							&& t.getArmyCount() > 1) {
						action.add(t);

						// second move
					} else if (action.size() >= 1 && !myTerritory(t)
							&& isAdjacent(t)) {
						action.add(t);
					}

				} else if (phase == 3 && myTerritory(t) && !locked.contains(t)) {

					// forces adjacent territory click
					if (action.size() >= 1 && isAdjacent(t)) {
						action.add(t);

					} else if (action.size() < 1)
						action.add(t);

				}

				// remove action
			} else {
				int temp = action.indexOf(t);
				action.remove(temp);
				System.out.println("Removed!");
			}

			setMessage();

			// when have 2 actions stored
			if (action.size() == 2) {
				takeAction();
			}
		}
	}

	public void buttonClick() {
		// free move
		if (phase == 2) {
			phase = 3;
			action.clear();
			setMessage();
		}

		// end turn
		else if (phase == 3) {
			advanceTurn();
		}
	}

	// Pre-condition: 2 actions stored
	private void takeAction() {

		System.out.println("Action!");
		if (phase == 2) {
			// pre-condition: First click is always your own territory
			// checks to make sure the second click is not your own
			// territory

			// Make attack!

			// must add stuff so you can choose how many armies you send

			Territory attacker = action.get(0);
			Territory defender = action.get(1);
			originT = attacker;
			destT = defender;

			needsBattleInput = true;
			playState.agui.showAttackGUI(originT, destT);

		} else if (phase == 3) {
			// pre-condition: First click is always your own territory
			// checks if second territory is your own
			if (myTerritory(action.get(1))) {
				// pop up window that lets you move armies
				Territory origin = action.get(0);
				Territory destination = action.get(1);

				originT = origin;
				destT = destination;

				originTArmy = originT.getArmyCount();
				destTArmy = destT.getArmyCount();

				needsNumberInput = true;
				totalArmyStore = destination.getArmyCount()
						+ origin.getArmyCount();

				playState.ns.create(1, totalArmyStore - 1, 0);

			}
		}

		action.clear();
		setMessage();
	}

	public void recievedNumberInput(boolean success) {
		needsNumberInput = false;
		if (success) {

			locked.add(originT);
			locked.add(destT);

			originT.setLocked(true);
			destT.setLocked(true);
		} else {

			originT.setArmyCount(originTArmy);
			destT.setArmyCount(destTArmy);

			originT.setLocked(false);
			destT.setLocked(false);

		}
		playState.ns.hide();
	}

	public void numberInputChanged() {
		System.out.println(playState.ns.getCounterValue());
		if (needsNumberInput) {
			destT.setArmyCount(playState.ns.getCounterValue());
			originT.setArmyCount(totalArmyStore
					- playState.ns.getCounterValue());
		}
	}

	public void battleGUIInput(boolean success, Battle b, int unused) {
		if (success) {
			needsBattleInput = false;
			if (b.getNewArmyCount() + unused >= 1) {
				originT.setArmyCount(b.getNewArmyCount() + unused);
			} else {
				originT.setArmyCount(1);
			}
			System.out.println(b.getNewArmyCount() + unused + " and "
					+ b.getOppNewArmyCount());
			destT.setArmyCount(b.getOppNewArmyCount());

			// if you win
			if (destT.getArmyCount() <= 0) {

				for (int i = 0; i < players.length; i++) {
					if (destT.getPlayerID() == players[i].getPlayerID()) {
						players[i].setTerritoryOwned(players[i]
								.getTerritoryOwned() - 1);
					}
				}

				destT.setPlayerID(currentPlayer.getPlayerID());
				destT.setDevelopment((int) destT.getDevelopment() / 2);
				destT.setArmyCount(originT.getArmyCount() - unused);
				originT.setArmyCount(unused + 1);
				currentPlayer.setTerritoryOwned(currentPlayer
						.getTerritoryOwned() + 1);

			}

			// win check
			for (int i = 0; i < players.length; i++) {
				if (players[i].getLost() || players[i].getTerritoryOwned() <= 0) {
					players[i].setLost(true);
				}

				if (checkWin()) {
					System.out.println("WIN");
				}
			}
		} else {
			needsBattleInput = false;
		}
	}

	// Pre-condition: can place armies
	private boolean placeArmy(Territory t) {
		if (myTerritory(t)) {
			t.setArmyCount(t.getArmyCount() + 1);
			placementArmies++;
			System.out.println(placementArmies + " vs "
					+ currentPlayer.getArmyGain());
			if (placementArmies == currentPlayer.getArmyGain()) {
				phase = 2;
				System.out.println("moving on now");
				setMessage();
			}

			return true;
		} else {
			System.out.println("Wrong territory!");
			return false;
		}
	}

	// Pre-condition: draft phase
	private boolean placeDraftArmy(Territory t) {
		if (t.getPlayerID() == 0) {
			t.setArmyCount(t.getArmyCount() + 1);
			t.setPlayerID(currentPlayer.getPlayerID());
			draftCounter--;
			currentPlayer
					.setTerritoryOwned(currentPlayer.getTerritoryOwned() + 1);
			return true;
		} else {
			System.out.println("Wrong territory!");
			return false;
		}
	}

	// checks if player owns the territory
	private boolean myTerritory(Territory t) {
		if (t.getPlayerID() == players[pointer].getPlayerID()) {
			return true;
		}
		return false;
	}

	// Pre-condition: action.size() == 1
	private boolean isAdjacent(Territory t) {
		// if in the same row
		if (action.get(0).getY() == t.getY()) {
			// check if next to each other
			if (action.get(0).getX() == t.getX() - 1
					|| action.get(0).getX() == t.getX() + 1) {
				return true;
			}
		}
		// if in same column
		else if (action.get(0).getX() == t.getX()) {
			// check if above/below each other
			// for triangle pointing up
			if (action.get(0).getDirection()
					&& action.get(0).getY() == t.getY() - 1) {
				return true;
			}
			// for triangle pointing down
			else if (!action.get(0).getDirection()
					&& action.get(0).getY() == t.getY() + 1) {
				return true;
			}
		}
		return false;
	}

	// ends current player's turn and moves onto the next player
	private void advanceTurn() {

		if (pointer >= 0) {

			currentPlayer.setCanPlay(false);

		}

		pointer++;

		if (pointer == players.length) {
			pointer = 0;
		}

		// resets variables

		currentPlayer = players[pointer];

		currentPlayer.setCanPlay(true); // TODO - check if this boolean is
		// necessary?

		placementArmies = 0;

		if (currentPlayer.getIsAI()) {
			currentPlayer.getTheAI().doTurn();
		}

		else {

			if (draftCounter <= 0) {
				phase = 1;
				for (int i = 0; i < map.length; i++) {
					for (int k = 0; k < map[0].length; k++) {
						if (map[i][k] instanceof Territory
								&& map[i][k].getPlayerID() == 0) {
							map[i][k].setArmyCount(1);
						}
					}
				}
			} else {
				phase = 0;
			}

			if (currentPlayer.getLost()) {
				advanceTurn();
			}

			action.clear();
			for (int i = 0; i < locked.size(); i++) {
				locked.get(i).setLocked(false);
			}
			locked.clear();
			setMessage();

			if (phase != 0) {
				for (int i = 0; i < map.length; i++) {
					for (int k = 0; k < map[0].length; k++) {
						if (map[i][k] instanceof Territory
								&& map[i][k].getPlayerID() == currentPlayer
										.getPlayerID()) {
							if (map[i][k].getDevelopment()
									+ board.getNumberOfOwnedAdjacentTerritories(
											i, k, currentPlayer.getPlayerID()) < devCap) {
								int sur = board
										.getNumberOfOwnedAdjacentTerritories(i,
												k, currentPlayer.getPlayerID());
								if (sur == 2) {
									map[i][k].setDevelopment(map[i][k]
											.getDevelopment() + 1);

								} else if (sur == 3) {
									map[i][k].setDevelopment(map[i][k]
											.getDevelopment() + 3);

								}
							} else {
								map[i][k].setDevelopment(devCap);
							}
						}
					}
				}
			}

			int devTer = 0;
			if (phase != 0) {
				for (int i = 0; i < map.length; i++) {
					for (int k = 0; k < map[0].length; k++) {
						if (map[i][k] instanceof Territory
								&& map[i][k].getPlayerID() == currentPlayer
										.getPlayerID()
								&& map[i][k].getDevelopment() == devCap) {
							devTer++;
						}
					}
				}
			}
			currentPlayer.setArmyGain(3 + devTer);
		}

	}

	private void setMessage() {
		if (phase == 0) {
			inst = "Draft! Pick your starting territories, player "
					+ players[pointer].getPlayerID() + "!";
		} else if (phase == 1) {
			inst = "Placement! Add armies to your territories, player "
					+ players[pointer].getPlayerID() + "!";
		} else if (phase == 2) {
			if (action.size() < 1) {
				inst = "Battle! Choose a territory to attack with, player "
						+ players[pointer].getPlayerID() + "!";
			} else if (action.size() >= 1) {
				inst = "Battle! Choose a territory to attack, player "
						+ players[pointer].getPlayerID() + "!";
			}
		} else if (phase == 3) {
			if (action.size() < 1) {
				inst = "Move! Choose a territory to move armies from, player "
						+ players[pointer].getPlayerID() + "!";
			} else if (action.size() >= 1) {
				inst = "Move! Choose a territory to armies to, player "
						+ players[pointer].getPlayerID() + "!";
			}
		}

		playState.setIZSet(inst);
	}

	public ArrayList<Territory> getActions() {
		return action;
	}

	private boolean checkWin() {
		// TODO
		if (phase != 0) {

			int id = currentPlayer.getPlayerID();

			for (int i = 0; i < map.length; i++) {
				for (int k = 0; k < map[0].length; k++) {

					if (map[i][k] instanceof Territory) {
						if (map[i][k].getPlayerID() != id
								&& map[i][k].getPlayerID() != 0) {
							return false;
						}

					}

				}
			}

			return true;
		}

		return false;

	}

	public int getPhase() {
		return phase;
	}

	public int getPlacementArmies() {
		return placementArmies;
	}

	public void setPlacementArmies(int pA) {
		placementArmies = pA;
	}
}
