package com.rational.progress;

import java.awt.Font;


import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;




public class SetupMenu extends BasicGameState {
	private Rectangle button1;
	private float x, y;
	private float scale;
	private StateBasedGame sbg;
	Font font;
	TrueTypeFont ttf;
	
	public SetupMenu(int id) {
		
		
		button1 = new Rectangle(300, 175, 200, 60);
		//reformRectangle();
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		// TODO Auto-generated method stub
		  this.sbg = sbg;
		
		  font = new Font("Verdana", Font.BOLD, 24);
		  ttf = new TrueTypeFont(font, true);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		Image land = null;
		// TODO Auto-generated method stub
		land = new Image("res/epic battle candidate10.jpg");
	    land.draw(0,0);
		float xpos = (x / 2) * scale, ypos = y * scale;
		if (x > 12) {
			xpos -= 100;
		} else {
			xpos += 60;
		}
		
		Color gray = Color.gray;
		gray.a = .2f;
		g.setColor(gray);
		gray.a = 1f;
		g.fill(button1);
		g.draw(button1);
		g.setColor(Color.black); 

		ttf.drawString(xpos + 303, ypos + 186, "BEGIN!");
		
		
//		 g.setColor(Color.white);
//		    g.drawString("Higher or Lower", 50, 10);
//		 
//		    g.drawString("1. Play Game", 50, 100);
//		    g.drawString("2. High Scores", 50, 120);
//		    g.drawString("3. Quit", 50, 140);
//		    
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		// TODO Auto-generated method stub
		Input i = gc.getInput();
		if (button1.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMousePressed(0)) {
				//System.out.println("123123");
				sbg.enterState(1, new FadeOutTransition(), new FadeInTransition());
			}
		}
	}

	@Override
	public int getID() {
		return 0;
	}

	
	
}
