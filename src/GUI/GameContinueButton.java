package GUI;

import java.awt.Font;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;

import com.rational.progress.GameManager;

public class GameContinueButton {
	Font font = new Font("Verdana", Font.BOLD, 18);
	TrueTypeFont ttf = new TrueTypeFont(font, true);
	private Rectangle r;
	private boolean showing = false;
	private GameManager gm;

	public GameContinueButton() {
		r = new Rectangle(670, 475, 150, 45);
	}

	public void show() {
		showing = true;
	}

	public void hide() {
		showing = false;
	}

	public void render(GameContainer gc, Graphics g) {
		if (showing) {
			g.setColor(Color.red.darker());
			g.fill(r);
			ttf.drawString(677, 484, "Continue>>", Color.white);
		}
	}

	public void update(GameContainer gc, int delta) {
		if (showing) {
			Input i = gc.getInput();
			if (r.contains(i.getMouseX(), i.getMouseY())) {
				if (i.isMousePressed(0)) {
					System.out.println("Button register");
					gm.buttonClick();
				}
			}
		}
	}

	public void setGM(GameManager gm) {
		this.gm = gm;
	}

}