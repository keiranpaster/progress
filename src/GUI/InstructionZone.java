package GUI;

import org.newdawn.slick.*;
import java.awt.Font;

public class InstructionZone {
	
	private String text;
	Font font = new Font("Verdana", Font.BOLD, 24);
	TrueTypeFont ttf = new TrueTypeFont(font, true);
	
	public InstructionZone() {
		text = "";
	}
	
	public void render(GameContainer gc, Graphics g) {
		g.setColor(Color.white);
		ttf.drawString(gc.getWidth() / 2 - ttf.getWidth(text) / 2, 10, text);
	}
	
	public void setInstructions(String text) {
		this.text = text;
	}
	
}
