package GUI;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import board.Territory;

import com.rational.progress.Battle;
import com.rational.progress.GameManager;

import java.awt.Font;

public class AttackGUI {
	
	private int x = 150, y = 150;
	private double opacity;
	Font font = new Font("Verdana", Font.PLAIN, 24);
	TrueTypeFont ttf = new TrueTypeFont(font, true);
	Font smallFont = new Font("Verdana", Font.BOLD, 15);
	TrueTypeFont sttf = new TrueTypeFont(smallFont, true);
	private final String title = "Prepare to Battle!!!";
	private Battle battle;
	private Territory attacker, defender;
	private Rectangle more, less, attack, deny,backgroundButton;
	 
	private int attackingArmies = 1;
	private boolean showing = false;
	private GameManager gm;
	private boolean updateBattle = false;
	Image backGround;
	Image attackImage;
	Image denyImage;
	
	
	public AttackGUI(GameManager gm) throws SlickException{
		
		less = new Rectangle(x + 40, y + 75, 30, 30);
		more = new Rectangle(x + 80, y + 75, 30, 30);
		attack = new Rectangle(x + 80, y + 200, 80, 20);
		deny = new Rectangle(x + 180, y + 200, 80, 20);
		backgroundButton = new Rectangle(y, x , 500, 295);
		this.gm = gm;
		attackImage = new Image( "res/attackButton.jpg");
		denyImage = new Image("res/waitButton.jpg");
		backGround = new Image("res/armyV4.jpg");
		battle = new Battle();

	}
	
	public void showAttackGUI(Territory a, Territory b) {
		attacker = a;
		defender = b;
		attackingArmies = attacker.getArmyCount() - 1;
		updateBattle = true;
		showing = true;
	}
	
	public void hideAttackGUI() {
		showing = false;
	}
	
	public void render(GameContainer gc, Graphics g) {
		if (showing) {
			// render window
			int width = gc.getWidth() - (x * 2);
			int height = gc.getHeight() - (y * 2);
			if (backGround != null) {
				
				if(opacity == 1){
			    g.drawImage(backGround,x,y,new Color(1f,1f,1f,1f));
				}
				if(opacity == 0.5){
				    g.drawImage(backGround,x,y,new Color(1f,1f,1f,0.5f));
					}
			}
			// render title
			ttf.drawString(x + (width / 2) - 120, y + 12, title, Color.black);
			// render two territories
			sttf.drawString(x + 30, y + 50, "Attacker: " + attackingArmies, Color.black);
			sttf.drawString(x + sttf.getWidth("Attacker: " + attackingArmies) + 40, y + 50, "Defender: " + defender.getArmyCount(), Color.black);
			// render number buttons
			g.setColor(Color.gray);
			g.fill(more);
			g.fill(less);
			sttf.drawString(more.getMinX() + 8, more.getMinY() + 5, "+", Color.black);
			sttf.drawString(less.getMinX() + 12, less.getMinY() + 5, "-", Color.black);
			// render attack deny buttons
			g.setColor(Color.green.darker());
			g.fill(attack);
			g.setColor(Color.red);
			g.fill(deny);
			attackImage.draw(x + 80, y + 200);
			denyImage.draw(x + 180, y + 200);
			sttf.drawString(attack.getCenterX() - 36, attack.getCenterY() - 10, "ATTACK!", Color.black);
			sttf.drawString(deny.getCenterX() - 22, deny.getCenterY() - 10, "WAIT", Color.black);
			// render percent win
			sttf.drawString(x + width - 225, y + 130, "Chance of Winning: " + battle.getBen(), Color.black); // daniel slacking off not finishing the battle class with getters...
			sttf.drawString(x + width - 225, y + 150, "W:(5/4)            L:(4/5)", Color.black);
		}
	}
	
	public void update(GameContainer gc, int delta) {
		if (showing) {
			Input i = gc.getInput();
			if ((backgroundButton).contains(i.getMouseX(), i.getMouseY())){
				opacity = 1;
			}
			else{
				opacity = 0.5;
			}
			if (less.contains(i.getMouseX(), i.getMouseY())) {
				if (i.isMousePressed(0) && attackingArmies > 1) {
					attackingArmies--;
					updateBattle = true;
				}
			}
			if (more.contains(i.getMouseX(), i.getMouseY())) {
				if (i.isMousePressed(0) && attackingArmies < attacker.getArmyCount() - 1) {
					attackingArmies++;
					updateBattle = true;
				}
			}
			if (updateBattle) {
				battle.calculate(attackingArmies, defender.getArmyCount());
				System.out.println("Attacking with: " + attackingArmies + " vs " + defender.getArmyCount());
			}
			if (attack.contains(i.getMouseX(), i.getMouseY())) {
				if (i.isMousePressed(0)) {
					gm.battleGUIInput(true, battle, attacker.getArmyCount() - attackingArmies - 1);
					showing = false;
				}
			}
			if (deny.contains(i.getMouseX(), i.getMouseY())) {
				if (i.isMousePressed(0)) {
					gm.battleGUIInput(false, battle, attacker.getArmyCount() - attackingArmies - 1);
					showing = false;
				}
			}
		}
		updateBattle = false;
	}
	
	/*
	 * 
	 * THINGS TO CHANGE ABOUT THIS TERRIBLE CLASS:
	 * brings in territories and constructs battle class
	 * uses updated battle class for real answers (reusable battle class pls)
	 * button pressing animations???
	 * 
	 */
	
}
