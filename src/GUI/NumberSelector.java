package GUI;

import java.awt.Font;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;

import com.rational.progress.GameManager;

import java.awt.Font;

public class NumberSelector {

	private Rectangle more, less, accept, deny;
	private final int x = 10, y = 100 , dist = 30;
	private int yAxis = 0;
	private boolean showing;
	Font font = new Font("Verdana", Font.BOLD, 15);
	Font font1 = new Font("Verdana", Font.ITALIC, 14);
	TrueTypeFont sttf = new TrueTypeFont(font1, true);
	TrueTypeFont ttf = new TrueTypeFont(font, true);
	private int counter, min, max;
	private GameManager gm;
	
	public NumberSelector(GameManager gm) {
		less = new Rectangle(x , y , dist + 30, dist);
		more = new Rectangle(x + dist + 30 , y, dist + 30, dist );
		accept = new Rectangle(x , y + dist, dist + 30, dist);
		deny = new Rectangle(x + dist + 30, y + dist, dist + 30, dist); 
		showing = false;
		this.gm = gm;
	}
	
	public void create(int min, int max, int start) {
		showing = true;
		this.counter = start;
		this.min = min;
		this.max = max;
	}
	
	public int getCounterValue() {
		return counter;
	}
	
	public void hide() {
		showing = false;
	}

		
	public void render(GameContainer gc, Graphics g) {
		if (showing) {
			g.setColor(Color.lightGray);
			g.fill(less);
			g.setColor(Color.gray);
			g.fill(more);
			g.setColor(Color.green.darker());
			g.fill(accept);
			g.setColor(Color.red.darker());
			g.fill(deny);
		
			ttf.drawString(less.getMinX() + 15, less.getMinY() + 4, "-", Color.black);
			ttf.drawString(more.getMinX() + 40, more.getMinY() + 4, "+", Color.black);
			sttf.drawString(accept.getMinX() + 6, accept.getMinY() + 2, "Accept  ", Color.black);
			sttf.drawString(deny.getMinX() + 10, deny.getMinY() + 2, "  Deny", Color.black);
		}
	}
	
	public void update(GameContainer gc, int delta) {
		if (showing) {
			Input i = gc.getInput();
			if (i.isMousePressed(0)) {
				if (less.contains(i.getMouseX(), i.getMouseY())) {
					if (counter > min) {
						counter--;
						gm.numberInputChanged();
					}
				}
				if (more.contains(i.getMouseX(), i.getMouseY())) {
					if (counter < max) {
						counter++;
						gm.numberInputChanged();
					}
				}
				if (accept.contains(i.getMouseX(), i.getMouseY())) {
					gm.recievedNumberInput(true);
				}
				if (deny.contains(i.getMouseX(), i.getMouseY())) {
					gm.recievedNumberInput(false);
				}
			}
		}
	}
		
}
