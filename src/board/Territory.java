package board;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;

import player.Player;
import com.rational.progress.GameManager;

public class Territory implements Comparable<Territory> {

	private Polygon polygon;
	private float scale;
	private boolean isHover = false;
	private float x, y, modx;
	private long popupTime;
	private boolean displayPopup = false;
	private boolean clickEffect = false;
	private boolean locked = false;

	private int playerID;
	private int armyCount;
	private int development;
	private GameManager gm;

	// false for down
	// true for up
	private boolean direction;

	private boolean collapseMode = false;
	private boolean highlight = false;

	private int heur;

	/**
	 * Constructs a Territory object, the triangle
	 * 
	 * @param player
	 *            - Player ID
	 * @param xpos
	 *            - x position in the grid
	 * @param ypos
	 *            - y position in the grid
	 */
	public Territory(int player, float xpos, float ypos) {
		scale = 60;
		this.x = xpos;
		this.y = ypos;
		this.modx = 0;
		polygon = new Polygon();
		reformPolygon();

		playerID = player;
		armyCount = 0;
		development = 0;

	}

	public void toggleHighlight() {
		highlight = !highlight;
	}

	public void highlightOff() {
		highlight = false;
	}

	/**
	 * Renders the Territory object
	 * 
	 * @param gc
	 *            - graphics stuff
	 * @param g
	 *            - graphics stuff
	 */

	public void render(GameContainer gc, Graphics g) {

		Color c = Player.getColorForPlayerID(playerID);
		if (gm.getActions().contains(this)) {
			c = c.brighter(.5f);
		} else if (clickEffect) {
			c = c.darker(.2f);
		} else if (locked) {
			c = c.darker(.5f);
		}

		g.setColor(c);
		g.fill(polygon);
		if (highlight) {
			g.setColor(Color.yellow);
		} else {
			g.setColor(Color.black);
		}
		g.draw(polygon);

		// renders the number of armies in a territory
		// LEAVE THE COORDS ALONE

		if ((x + y) % 2 == 1) { // down triangles

			if (armyCount < 10) { // 1 digit

				armyText(g, 9, 10);

			} else if (armyCount < 100) { // 2 digits

				armyText(g, 18, 10);

			} else { // 3/4

				armyText(g, 28, 10);

			}

			if (development >= GameManager.devCap) {
				developmentStar(g);
			}

		} else { // up triangles

			if (armyCount < 10) { // 1 digit

				armyText(g, 9, 10);

			} else if (armyCount < 100) // 2 digits
			{
				armyText(g, 18, 10);

			} else
			// 3 digits, 4 digits unlikely
			{
				armyText(g, 28, 10);

			}
			if (development >= GameManager.devCap) {
				developmentStar(g);
			}

		}
	}

	public void collapse() {
		collapseMode = !collapseMode;
	}

	/**
	 * Renders popups
	 * 
	 * @param gc
	 *            - GameContainer object
	 * @param g
	 *            - Graphics object
	 */
	public void renderPopup(GameContainer gc, Graphics g) {
		if (displayPopup) {
			float xpos = (x / 2) * scale, ypos = y * scale;
			if (x > 12) {
				xpos -= 100;
			} else {
				xpos += 60;
			}
			g.setColor(Color.white);
			g.fillRect(xpos, ypos + 15, 100, 30);
			g.setColor(Color.black);
			g.drawRect(xpos, ypos + 15, 100, 30);
			g.drawString(development + " DP", xpos + 29, ypos + 21);
		}
	}

	/**
	 * Updates.
	 * 
	 * @param gc
	 * @param delta
	 */
	public void update(GameContainer gc, int delta) {
		Input i = gc.getInput();
		if (polygon.contains(i.getMouseX(), i.getMouseY())) {
			if (i.isMouseButtonDown(0)) {
				clickEffect = true;
			} else {
				clickEffect = false;
			}
			if (!isHover) {
				popupTime = System.currentTimeMillis() + 500;
			}
			isHover = true;

			if (i.isMousePressed(0)) {
				gm.territoryClick(this);
			}
		} else {
			isHover = false;
			displayPopup = false;
			clickEffect = false;
		}
		reformPolygon();
		if (isHover) {
			if (System.currentTimeMillis() > popupTime) {
				displayPopup = true;
			}
		}
		if (collapseMode) {
			if ((x + y) % 2 == 1) {
				modx += 1f * delta;
			} else {
				modx -= 1f * delta;
			}
		} else {
			modx = 0;
		}
	}

	/**
	 * 
	 * Renders the number of armies of a territory, approximately centered in
	 * triangles
	 * 
	 * @param g
	 *            - Graphics object
	 * @param width
	 *            - width of the text
	 * @param height
	 *            - height of the text
	 * @param dir
	 *            - direction of the triangle
	 */
	private void armyText(Graphics g, int width, int height) {
		int xpos = (int) (((x / 2) * scale) + scale * 0.5);

		// true is up
		if (direction) {

			g.drawString(armyCount + "", xpos - (int) (.5 * width), (int) y
					* scale + (int) (.5 * (scale + height * .5)));
		}

		// false is down
		else {
			g.drawString(armyCount + "", xpos - (int) (.5 * width), (int) y
					* scale + (int) (.5 * (scale - height * 4)));

		}
	}

	/**
	 * Draws the development star
	 * 
	 * @param g
	 *            - Graphics object
	 * @param dir
	 *            - direction of triangle
	 */
	private void developmentStar(Graphics g) {
		int xpos = (int) (((x / 2) * scale) + scale * 0.5);
		int sw = 7; // sw = star width
		int sh = 10; // sh = star height

		// true is up
		if (direction) {
			g.drawString("*", xpos - (int) (sw * .5) - 2, (int) y * scale
					+ (int) (.5 * scale - 1.5 * sh));

		}

		// false is down
		else {

			g.drawString("*", xpos - (int) (.5 * sw) - 2, (int) y * scale
					+ (int) (.5 * (scale + .5 * sh)));

		}

	}

	/**
	 * Draws the triangles?
	 */
	private void reformPolygon() {
		float xpos = x / 2;
		polygon = new Polygon();
		if ((x + y) % 2 == 1) { // down triangles
			polygon.addPoint(xpos * scale + modx, y * scale + 0); // top left
			polygon.addPoint(xpos * scale + scale + modx, y * scale + 0); // top
			// right
			polygon.addPoint(xpos * scale + (int) (.5 * scale) + modx, y
					* scale + scale); // bottom
			direction = false;
		} else { // up triangles
			polygon.addPoint(xpos * scale + modx, y * scale + scale); // bottom
																		// left
			polygon.addPoint(xpos * scale + scale + modx, y * scale + scale); // bottom
			// right
			polygon.addPoint(xpos * scale + (int) (.5 * scale) + modx, y
					* scale); // bottom
			direction = true;
		}
	}

	/*
	 * Getter and setter methods
	 */
	public int getPlayerID() {
		return playerID;
	}

	public int getArmyCount() {
		return armyCount;
	}

	public int getDevelopment() {
		return development;
	}

	public void setPlayerID(int player) {
		playerID = player;
	}

	public void setArmyCount(int army) {
		armyCount = army;
	}

	public void setDevelopment(int develop) {
		development = develop;
	}

	public void setGM(GameManager gm) {
		this.gm = gm;
	}

	public String toString() {
		return ((int) x + " " + (int) y);
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public boolean getDirection() {
		return direction;
	}

	public void setLocked(boolean lock) {
		locked = lock;
	}

	public int getHeur() {
		return heur;
	}

	public void setHeur(int h) {
		heur = h;
	}

	@Override
	public int compareTo(Territory t) {
		// negative number if this comes before t
		// positive if this comes after t

		if (this.heur < t.getHeur()) {
			return 1; // TODO
		} else if (this.heur > t.getHeur()) {
			return -1; // TODO
		}

		return 0;
	}
	
	public boolean equals(Object o) {
		Territory t = (Territory)o;
		if (t.getX() == this.getX() && t.getY() == this.getY()) {
			return true;
		}
		return false;
	}

}
