package board;

public class BoardGenerator {

	private int players, width, height;
	
	public BoardGenerator(int players, int width, int height) {
		this.players = players;
		this.width = width;
		this.height = height;
	}
	
	public void generateMap(Territory[][] map) {
		for (int i = 0; i < players; i++) {
			travelWorm(map, (int)width / 2, (int)height / 2, 0);
		}
	}
	//
	private void travelWorm(Territory[][] map, int x, int y, int dur) {
		if (x != 0 && x != width && y != 0 && y != height && dur < 100) {
			map[x][y] = new Territory(0, x, y);
			int dir = (int) (Math.random() * 3 + 1);
			if (dir == 1) {
				travelWorm(map, x + 1, y, ++dur);
			} else if (dir == 2) {
				travelWorm(map, x - 1, y, ++dur);
			} else if (dir == 3) {
				if ((x + y) % 2 == 1)  {
					travelWorm(map, x, y - 1, ++dur);
				} else {
					travelWorm(map, x, y + 1, ++dur);
				}
			}
			dur++;
			
		} else {
			
		}
	}
		
}
