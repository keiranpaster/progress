package board;

import java.util.ArrayList;

import org.newdawn.slick.*;

public class Board {

	private final int width = 25;
	private final int height = 8;

	private Territory[][] map;

	public Board(int players) {
		map = new Territory[width][height];
		BoardGenerator bg = new BoardGenerator(players, width, height);
		bg.generateMap(map);
	}

	public void render(GameContainer gc, Graphics g) {

		for (int i = 0; i < width; i++) {
			for (int c = 0; c < height; c++) {
				if (map[i][c] != null) {
					map[i][c].render(gc, g);
				}
			}
		}
		for (int i = 0; i < width; i++) {
			for (int c = 0; c < height; c++) {
				if (map[i][c] != null) {
					map[i][c].renderPopup(gc, g);
				}
			}
		}

	}

	public void update(GameContainer gc, int delta) {
		for (int i = 0; i < width; i++) {
			for (int c = 0; c < height; c++) {
				if (map[i][c] != null) {
					map[i][c].update(gc, delta);
				}
			}
		}
	}

	// TODO
	public ArrayList<Territory> getAdjacentTerritories(int x, int y) {
		ArrayList<Territory> ts = new ArrayList<Territory>();
		try {
			if (map[x][y] != null) {
				boolean dir = map[x][y].getDirection();
	
				if (map[x - 1][y] != null) {
					ts.add(map[x - 1][y]);
				}
	
				if (map[x + 1][y] != null) {
					ts.add(map[x + 1][y]);
				}
	
				if (dir) {
					if (map[x][y + 1] != null) {
						ts.add(map[x][y + 1]);
					}
				} else {
					if (map[x][y - 1] != null) {
						ts.add(map[x][y - 1]);
					}
				}
			}
		} catch (Exception e) {
			
		}
		return ts;
	}

	// TODO
	public int getNumberOfOwnedAdjacentTerritories(int x, int y, int id) {
		ArrayList<Territory> ts = this.getAdjacentTerritories(x, y);
		int count = 0;
		for (Territory t : ts) {
			if (t.getPlayerID() == id) {
				count++;
			}
		}
		return count;
	}

	public Territory[][] getMap() {
		return map;
	}

	public void collapse() {
		for (int i = 0; i < width; i++) {
			for (int c = 0; c < height; c++) {
				if (map[i][c] != null) {
					map[i][c].collapse();
				}
			}
		}
	}

}
