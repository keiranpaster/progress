package player;

import java.util.ArrayList;

import org.newdawn.slick.*;

import AI.ProgressAI;
import board.Territory;

public class Player {

	private int playerID;
	private int territoryOwned;
	private int armyGain;
	private int boxHeight = 30;
	private int startX = 0, width = 0;
	private boolean canPlay;
	private boolean lost;
	private boolean isAI = false;
	private ProgressAI theAI;

	private ArrayList<Territory> ownedTerritories;

	public Player(int playerID, int players, int w) {
		this.playerID = playerID;
		territoryOwned = 0;
		armyGain = 3;
		width = (w / players);
		startX = (playerID - 1) * width;
		canPlay = false;
		lost = false;
		ownedTerritories = new ArrayList<Territory>();
	}

	public void renderPlayerBox(GameContainer gc, Graphics g) {

		if (lost) {
			g.setColor(getColorForPlayerID(playerID).darker(.5f));

		} else {
			g.setColor(getColorForPlayerID(playerID));
		}
		g.fillRect(startX, gc.getHeight() - boxHeight, width, boxHeight);
		g.setColor(Color.black);
		g.drawString("Player " + playerID, startX + width / 2 - 35,
				gc.getHeight() - boxHeight + 5);
		g.drawString("Territory owned: " + territoryOwned + "\nArmy Gain: "
				+ armyGain, startX + width / 2 - 84, gc.getHeight() - boxHeight
				+ 30);
	}

	public void update(GameContainer gc, int delta) {
		Input i = gc.getInput();
		if (canPlay || i.getMouseY() > gc.getHeight() - boxHeight
				&& i.getMouseX() > startX && i.getMouseX() < startX + width) {
			if (boxHeight < 80) {
				boxHeight += 1 * delta;
			} else {
				boxHeight = 80;
			}
		} else {
			if (boxHeight > 30) {
				boxHeight -= 1 * delta;
			} else {
				boxHeight = 30;
			}
		}
	}

	public static Color getColorForPlayerID(int playerID) {
		if (playerID == 0) {
			return Color.gray;
		} else if (playerID == 1) {
			// red
			return new Color(238, 99, 99);
		} else if (playerID == 2) {
			// green
			return new Color(124, 205, 124);
		} else if (playerID == 3) {
			// yellow
			return new Color(255, 236, 139);
		} else if (playerID == 4) {
			// blue
			return new Color(135, 206, 250);
		}
		return Color.white;
	}

	public int getPlayerID() {
		return playerID;
	}

	public void setCanPlay(boolean cP) {
		canPlay = cP;
	}

	public int getArmyGain() {
		return armyGain;
	}

	public void setArmyGain(int aG) {
		armyGain = aG;
	}

	public int getTerritoryOwned() {
		return territoryOwned;
	}

	public void setTerritoryOwned(int to) {
		territoryOwned = to;
	}

	public boolean getLost() {
		return lost;
	}

	public void setLost(boolean lose) {
		lost = lose;
	}

	public boolean getIsAI() {
		return isAI;
	}
	
	public void setAI(boolean AI) {
		isAI = AI;
	}
	
	public ProgressAI getTheAI() {
		return theAI;
	}

	public void setTheAI(ProgressAI AI) {
		theAI = AI;
	}

}

/*
 * 
 * player a 10 - ->9 player b 9 - ->10
 */
